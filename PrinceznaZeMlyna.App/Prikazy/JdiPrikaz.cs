﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinceznaZeMlyna.Model;
using PrinceznaZeMlyna.Model.Interakce;
namespace PrinceznaZeMlyna.App.Prikazy
{
    public class JdiPrikaz : iPrikaz
    {
        private Mistnost _jdiDo;
        public JdiPrikaz(Mistnost jdiDo)
        {
            _jdiDo = jdiDo;
        }

        public bool ProvedPrikaz()
        {
            if (Hra.AktualniMistnost.SoudedniMistnosti.Contains(_jdiDo))
            {
                VykonejMetodu.ProvedInteract(new ZpravaInterakce($"{Model.Properties.Resources.EnterToLabel} {_jdiDo.Nazev}"));
                VykonejMetodu.ProvedInteract(new JdiInterakce(_jdiDo));
                List<abstractInterakce> returns = _jdiDo.UseInteract();
                Console.WriteLine(returns.Count());
                foreach (abstractInterakce interact in returns)
                {
                    VykonejMetodu.ProvedInteract(interact);
                }
                return true;
            }
            return false;
        }
    }
}